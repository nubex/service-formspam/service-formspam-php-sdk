<?php

namespace Nubex\FormSpamService;

use GuzzleHttp\Client as HttpClient;

class Facade
{
    private $apiKey;
    private $apiUrl;
    private $client = null;
    private $httpClient = null;
    private $siteCode;
    private $ip;

    /**
     * @param string $apiUrl
     * @param string $apiKey
     */
    public function __construct($apiUrl, $apiKey, $siteCode, $ip)
    {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
        $this->siteCode = $siteCode;
        $this->ip = $ip;
    }

    /**
     * @param HttpClient $httpClient
     * @return Facade
     */
    public function setHttpClient(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
        return $this;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        if ($this->httpClient === null) {
            $this->httpClient = new HttpClient();
        }
        return $this->httpClient;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        if ($this->client === null) {
            $httpClient = $this->getHttpClient();
            $this->client = new Client($this->apiUrl, $this->apiKey, $httpClient);
            $this->client->setHttpOption('http_errors', false);
            $this->client->setHttpOption('timeout', 3.0);

        }
        return $this->client;
    }

    /**
     * @return FormSpamService
     */
    public function getService()
    {
        return new FormSpamService($this->getClient(), $this->siteCode, $this->ip);
    }

}
