<?php

namespace Nubex\FormSpamService;


class FormSpamService
{
    /**
     * @var Client
     */
    private $client;
    private $siteCode;
    private $ip;

    public function __construct(Client $client, $siteCode, $ip)
    {
        $this->client = $client;
        $this->siteCode = $siteCode;
        $this->ip = $ip;
    }

    public function showForm($formId)
    {
        $requestData = array(
            'siteCode' => $this->siteCode,
            'ip' => $this->ip,
            'externalId' => $formId
        );

        $result = $this->client->put('form', $requestData);
        if ($result['success'] === true) {
            return $result['data']['token'];
        }
        return false;
    }

    public function applyForm($formId, $fields, $token)
    {
        $requestData = array(
            'siteCode' => $this->siteCode,
            'ip' => $this->ip,
            'externalId' => $formId,
            'fields' => $fields,
        );

        $result = $this->client->post('form/'.$token.'/apply', $requestData);
        if ($result['success'] === true) {
            return $result['data']['result'];
        }
        return false;
    }

}