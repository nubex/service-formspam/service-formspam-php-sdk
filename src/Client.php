<?php

namespace Nubex\FormSpamService;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\GuzzleException;

class Client
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * @var array
     */
    protected $httpOptions = [];

    /**
     * @var Request
     */
    protected $lastRequest = null;

    /**
     * @var Response
     */
    protected $lastResponse = null;

    /**
     * @param $baseUrl
     * @param $apiKey
     * @param ClientInterface $httpClient
     */
    public function __construct($baseUrl, $apiKey, ClientInterface $httpClient)
    {
        $this->setBaseUrl($baseUrl);
        $this->apiKey = $apiKey;
        $this->httpClient = $httpClient;
    }

    /**
     * @return array
     */
    public function getHttpOptions()
    {
        return $this->httpOptions;
    }

    /**
     * @param array $options
     */
    public function setHttpOptions(array $options = array())
    {
        $this->httpOptions = $options;
    }

    /**
     * @param string $name
     * @param $value
     */
    public function setHttpOption($name, $value)
    {
        $this->httpOptions[$name] = $value;
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return array|mixed
     */
    public function get($endpoint, array $params = [])
    {
        return $this->jsonQuery($endpoint, $params, self::METHOD_GET);
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return array|mixed
     */
    public function post($endpoint, array $params = [])
    {
        return $this->jsonQuery($endpoint, $params, self::METHOD_POST);
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return array|mixed
     */
    public function put($endpoint, array $params = [])
    {
        return $this->jsonQuery($endpoint, $params, self::METHOD_PUT);
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return array|mixed
     */
    public function delete($endpoint, array $params = [])
    {
        return $this->jsonQuery($endpoint, $params, self::METHOD_DELETE);
    }

    /**
     * @return Request
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    /**
     * @return Response
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @param string $url
     */
    protected function setBaseUrl($url)
    {
        $this->baseUrl = preg_replace('|/+$|', '', $url);
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @param string $method
     * @return array|mixed
     */
    protected function jsonQuery($endpoint, array $params = [], $method = self::METHOD_GET)
    {
        return $this->makeRequest(
            $method,
            $endpoint,
            0 < count($params) ? json_encode($params) : null,
            'application/json'
        );
    }

    /**
     * @param string $endpoint
     * @return string
     */
    protected function prepareUri($endpoint)
    {
        return $this->baseUrl . '/' . $endpoint;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param string $body
     * @param string $reqContentType
     * @return array|mixed
     */
    protected function makeRequest($method, $endpoint, $body, $reqContentType = 'application/octet-stream')
    {
        $oldErrorReportingLevel = error_reporting(E_ERROR);
        $uri = $this->prepareUri($endpoint);
        $headers = [
            'Content-Type' => $reqContentType,
            'Accept' => 'application/json',
            'X-Api-Key' => $this->apiKey,
        ];
        $this->lastResponse = null;
        $this->lastRequest = new Request($method, $uri, $headers, $body);

        try {
            $this->lastResponse = $this->httpClient->send($this->lastRequest, $this->httpOptions);
        } catch (ServerException $exception) {
            $responseBody = $exception->getResponse()->getBody();
            $result = ['success' => false, 'msg' => $exception, 'body' => (string)$responseBody];
            error_reporting($oldErrorReportingLevel);
            return $result;
        } catch (GuzzleException $exception) {
            $result = ['success' => false, 'msg' => $exception->getMessage()];
            error_reporting($oldErrorReportingLevel);
            return $result;
        }

        error_reporting($oldErrorReportingLevel);

        $contentType = $this->lastResponse->getHeader('content-type');
        if (count($contentType) !== 1) {
            $result = ['success' => false, 'msg' => 'Bad response header Content-type'];
            return $result;
        }

        $contentType = array_pop($contentType);

        $body = (string)$this->lastResponse->getBody();

        if (strpos($contentType, 'application/json') === false) {
            $result = ['success' => false, 'msg' => 'We need response content type: application/json', 'body' => $body];
            return $result;
        }

        if ($body === '') {
            $result = ['success' => false, 'msg' => 'Empty body in response'];
            return $result;
        }

        $data = json_decode($body, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $result = [
                'success' => false,
                'msg' => 'Error parsing response: ' . json_last_error_msg() . 'Body: ' . $body
            ];
            return $result;
        }

        $result = [
            'success' => true,
            'data' => $data,
        ];
        $result['httpStatusCode'] = $this->lastResponse->getStatusCode();

        return $result;
    }
}
