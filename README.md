
PHP SDK для работы с Nubex FormSpam Service
=================

PHP библиотека для работы с сервисом Nubex FormSpam Service с помощью REST API


## Установка для разработки

``` sh
git clone git@gitlab.com:nubex/service-formspam/service-formspam-php-sdk.git
cd service-filestorage-php-sdk
composer install
```

## Установка для использования

Настраиваем в composer-е репозитрий для загрузки библиотеки
```  
"repositories": [
      {
         "type": "vcs",
         "url": "git@gitlab.com:nubex/service-formspam/service-formspam-php-sdk.git"
      },
  ]
```
Подключаем в свой composer файл

``` text
composer require nubex/service-formspam-php-sdk                 
```

## Использование

``` php
$client = new \Nubex\FileService\Client('http://127.0.0.1:8090/api', API_KEY, , $_ENV['SITE_CODE'], $_SERVER['REMOTE_ADDR']);

$formSpamService = $facade->getService();
$token = $formSpamService->showForm(22);

if ($token!==false) {
   $applyResult = $formSpamService->applyForm(22, ['name'=>'User fio', 'email'=>'support@hell.ru', 'login'=>'Umbra!'], $token);	
}
```


